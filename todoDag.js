/**
 *  RootGroup->Vertex->Group->Vertecis
 *  parents are stored in a Set
 */

'use strict'
const DAG = require('functional-dag')

class ArrayMap extends Array {
  constructor (arrayMap) {
    if (!(arrayMap instanceof ArrayMap) && arrayMap instanceof Array) {
      super(...arrayMap)
    } else {
      // copy constuctor for maps
      super()
      if (arrayMap) {
        for (let item of arrayMap) {
          this[item[0]] = item[1]
        }
      }
    }
  }

  get (i) {
    return this[i]
  }
  set (i, value) {
    this[i] = value
  }

  delete (i) {
    this.splice(i, 1)
  }

  * [Symbol.iterator] () {
    let i = 0
    for (let item of super[Symbol.iterator]()) {
      yield [i, item]
      i++
    }
  }

  forEach (fn) {
    return [...this].map(function (el) {
      return el[1][1]
    }).forEach(fn)
  }
}

module.exports = class TodoDag extends DAG {
  static Edges (edges) {
    return new ArrayMap(edges)
  }

  pushEdge (vertex) {
    let newVertex = new this.constructor(this)
    newVertex._edges.push(vertex)
    return newVertex
  }
}
