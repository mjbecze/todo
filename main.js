'use strict'
const styles = require('./main.css')
const localStorageSync = require('./localStorage')
const Boil = require('./boilerplate.js')
const Item = require('./item.js')
const Group = require('./group.js')
const svg = require('virtual-dom/virtual-hyperscript/svg')

// setup boulerplate code
let boiler = new Boil({
  stateHandler: stateHandler,
  initState: getInitState(localStorageSync),
  styles: styles,
  render: render,
  rootEl: document.body
})

let store = boiler.store
localStorageSync.save(store)

// load the initail state from someWhere
function getInitState (provider) {
  return provider.load() || new Group()
}

function stateHandler (state, action) {
  window.state = state
  let item
  switch (action.type) {
    case 'addItem':
      item = new Item(action.description)
      let vertex = action.vertex
      if (vertex) {
        let newV = vertex.pushEdge(item)
        state = state.setVertex(vertex, newV)
      } else {
        state = state.pushEdge(item)
      }
      break
    case 'move':
      let movedVert = action.from
      let toVert = action.to
      state = state.delVertex(movedVert)

      if (toVert instanceof Group) {
        let newToVert = toVert.pushEdge(movedVert)
        state = state.setVertex(toVert, newToVert)
      }

      // create a new group
      if (!toVert.edges.length) {
        let group
        group = new Group()
        group = group.pushEdge(movedVert)
        let newToVert = toVert.pushEdge(group)
        state = state.setVertex(toVert, newToVert)
      }
      break
    case 'update':
      state = state.setVertex(action.vertex, action.update)
      break
    case 'delete':
      state = state.delVertex(action.vertex)
      break
    case 'toggle':
      let newVertex = action.vertex.toggle()
      state = state.setVertex(action.vertex, newVertex)
      break
    case 'createEdge':
      let group = action.to
      item = action.from
      let newVert = item.pushEdge(group)
      state = state.setVertex(item, newVert)
      break
  }
  return state
}

function render (state) {
  let self = this
  return this.h('div', [
    svg('svg', {
      width: '100%',
      height: '100%'
    },
    renderEdges(state)),
    this.h('input', {
      type: 'text',
      onkeypress: onItemEntered
    }),
    renderGroups([...state])
  ])

  function renderGroups (nodes) {
    return nodes.map(function (vertex) {
      vertex = vertex[1]
      if (vertex instanceof Group) {
        return vertex.render(self.h, self.store)
      }
    })
  }

  function renderEdges (state) {
    let lines = []
    state.iterate({
      accumulate: function (name, vertex, accum) {
        // we are at the root
        if (name === undefined) {
          accum = {}
        }
        if (vertex instanceof Item) {
          return {
            name: name,
            item: vertex,
            group: accum.group
          }
        } else {
          return {
            name: accum.name,
            item: accum.item,
            parentGroup: accum.group,
            group: vertex
          }
        }
      },
      aggregate: function * (name, vertex, accum) {
        if (name !== undefined && vertex instanceof Group && accum) {
          let xOffset = accum.parentGroup.getWidth()
          let yOffset = 12
          lines.push(
            svg('line', {
              x1: Number(accum.parentGroup.value.x.slice(0, -2)) + xOffset - 10,
              y1: Number(accum.parentGroup.value.y.slice(0, -2)) + accum.name * 20 + yOffset,
              x2: vertex.value.x.slice(0, -2) - 5,
              y2: vertex.value.y.slice(0, -2),
              stroke: 'black'
            })
          )
        }
      }
    }).next()

    return lines
  }
}

function onItemEntered (event) {
  event.stopPropagation()

  let value = event.target.value
  // on enter
  if (event.keyCode === 13 && value !== '') {
    let el = document.querySelectorAll('.' + styles.selected)
    let vertex
    if (el.length) {
      el = el[0]
      vertex = el.getVertex()
    }

    store.dispatch({
      type: 'addItem',
      vertex: vertex,
      description: value
    })

    // clear the input box
    event.target.value = ''
  }
}

// add global key bindings
window.addEventListener('keydown', function (event) {
  // delete
  if (event.keyCode === 46) {
    let el = document.querySelectorAll('.' + styles.selected)
    if (el.length) {
      el = el[0]
      el.classList.remove(styles.selected)
      store.dispatch({
        type: 'delete',
        vertex: el.getVertex()
      })
    }
  }
})
