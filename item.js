'use strict'
const TodoDag = require('./todoDag.js')
const styles = require('./main.css')

let draggedVertex

module.exports = class Item extends TodoDag {
  constructor (vertex) {
    super(vertex)
    if (!vertex || typeof vertex === 'string') {
      this._value = {
        description: vertex,
        complete: false
      }
    }
  }

  onClick (event) {
    let els = document.getElementsByClassName(styles.selected)
    if (els.length) {
      els[0].classList.remove(styles.selected)
    }
    event.target.classList.toggle(styles.selected)
  }

  toggle () {
    let newVertex = this.setValue({
      complete: !this._value.complete,
      description: this._value.description
    })

    return newVertex
  }

  render (h, store) {
    let self = this
    return h('li.item', {
      draggable: 'true',
      onclick: this.onClick,
      ondragover: function (ev) {
        ev.preventDefault()
        ev.stopPropagation()
        this.classList.add(styles.over)
      },
      ondrop: function (ev) {
        ev.preventDefault()
        ev.stopPropagation()
        this.classList.remove(styles.over)
        store.dispatch({
          type: 'move',
          from: draggedVertex,
          to: self
        })
      },
      ondragleave: function (ev) {
        this.classList.remove(styles.over)
      },
      ondragstart: function (ev) {
        // ev.stopPropagation()
        draggedVertex = self
      },
      getVertex: function () {
        return self
      }
    }, [
      h('input', {
        type: 'checkbox',
        checked: this._value.complete,
        onclick: function () {
          store.dispatch({
            type: 'toggle',
            vertex: self
          })
        }
      }),
      h('span', {
        getVertex: function () {
          return self
        }
      }, this._value.description)
    ])
  }
}
