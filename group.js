'use strict'
const TodoDag = require('./todoDag.js')
const styles = require('./main.css')

let draggedItem
let ctrlPressed = false

window.addEventListener('keydown', function (event) {
  if (event.which === 17) { // ctrl
    console.log('on');
    ctrlPressed = true
  }
})

window.addEventListener('keyup', function (event) {
  if (event.which === 17) { // ctrl
    ctrlPressed = false
    console.log('off');
  }
})

module.exports = class Group extends TodoDag {
  constructor (vertex) {
    super(vertex)
    this.id = Math.random().toString()
    this.lastOffsetWidth = 0
    if (!vertex) {
      this._value = {
        x: '0px',
        y: '0px'
      }
    } else if (vertex instanceof TodoDag) {
      this.id = vertex.id
      this.lastOffsetWidth = vertex.lastOffsetWidth
    }
  }

  copy () {
    let id = this.id
    // console.log('copy', this.lastOffsetWidth);
    let newV = super.copy(this)
    newV.id = id
    newV.lastOffsetWidth = this.lastOffsetWidth
    return newV
  }

  getWidth () {
    let dom = this.getDom()
    if (dom) {
      this.lastOffsetWidth = dom.offsetWidth
      return this.lastOffsetWidth
    } else {
      return this.lastOffsetWidth
    }
  }
  getDom () {
    return document.getElementById(this.id)
  }

  onClick (event) {
    let els = document.getElementsByClassName(styles.selected)
    if (els.length) {
      els[0].classList.remove(styles.selected)
    }
    event.target.classList.toggle(styles.selected)
  }

  render (h, store, path) {
    let self = this
    if (!path) {
      path = []
    }
    return h('ul.item', {
      id: this.id,
      onclick: this.onClick,
      draggable: 'true',
      style: {
        position: 'absolute',
        left: self.value.x,
        top: self.value.y
      },
      getVertex: function () {
        return self
      },
      ondragover: function (ev) {
        ev.preventDefault()
        ev.stopPropagation()
        this.classList.add(styles.over)
      },

      ondragleave: function (ev) {
        this.classList.remove(styles.over)
      },
      ondragend: function (ev) {
        if (!draggedItem) {
          this.classList.remove(styles.over)
          // event.preventDefault()
          let newGroup = self.setValue({
            x: (ev.x + self.leftOffset) + 'px',
            // 21? halp
            y: (ev.y + self.topOffset - 21) + 'px'
          })

          store.dispatch({
            type: 'update',
            vertex: self,
            update: newGroup
          })
        }
      },
      ondrop: function (ev) {
        this.classList.remove(styles.over)
        if (draggedItem) {
          if (ctrlPressed) {
            store.dispatch({
              type: 'createEdge',
              from: draggedItem,
              to: self
            })
          } else {
            store.dispatch({
              type: 'move',
              from: draggedItem,
              to: self
            })
          }
        }
      },
      ondragstart: function (ev) {
        let vertex = ev.target.getVertex()
        // always clear the dragged item
        draggedItem = null

        if (vertex instanceof Group) {
          let rect = ev.target.getBoundingClientRect()
          self.topOffset = rect.top - ev.y
          self.leftOffset = rect.left - ev.x
        } else {
          draggedItem = vertex
        }
      }
    }, this._edges.map(function (item, i) {
      return item.render(h, store, path.concat[i])
    }))
  }
}
