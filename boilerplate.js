'use strict'
const redux = require('redux')
const mainLoop = require('main-loop')
const vdom = require('virtual-dom')
const hyperstyles = require('hyperstyles')
require('dom-delegator')

let Boil = module.exports = function (opts) {
  this.setupHyper(opts.styles)
  this.setupRedux(opts.stateHandler, opts.initState)
  this.setupMainLoop(opts.render, opts.rootEl, opts.initState)
}

Boil.prototype.setupHyper = function (styles) {
  this.h = hyperstyles(vdom.h, styles)
}

// setup
Boil.prototype.setupRedux = function (stateHandler, initState) {
  this.store = redux.createStore(stateHandler, initState)
}

// setup mainloopdedupe
Boil.prototype.setupMainLoop = function (render, rootEl, initState) {
  let self = this
  let loop = mainLoop(this.store.getState(), render.bind(this), vdom)
  rootEl.appendChild(loop.target)

  this.store.subscribe(() =>
    loop.update(self.store.getState())
  )

  loop.update(initState)
}
