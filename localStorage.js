'use strict'
const LOC = 'todo_data'
const Group = require('./group.js')
const Item = require('./item.js')
const Vertex = require('generic-digraph')

class Factory extends Vertex {
  constructor (vert) {
    super()
    if (vert.value.x) {
      return new Group(vert)
    } else {
      return new Item(vert)
    }
  }

}

module.exports.save = function (store) {
  window.onbeforeunload = () => {
    console.log(JSON.stringify(store.getState().toJSON()))
    localStorage.setItem(LOC, JSON.stringify(store.getState().toJSON()))
  }
}

module.exports.load = function () {
  let nodes = JSON.parse(localStorage.getItem(LOC))
  if(nodes){
    return Factory.fromJSON(nodes)
  }
}
